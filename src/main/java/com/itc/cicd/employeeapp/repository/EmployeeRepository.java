package com.itc.cicd.employeeapp.repository;

import com.itc.cicd.employeeapp.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
