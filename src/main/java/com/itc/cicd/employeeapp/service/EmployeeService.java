package com.itc.cicd.employeeapp.service;

import com.itc.cicd.employeeapp.model.Employee;
import com.itc.cicd.employeeapp.repository.EmployeeRepository;

import java.util.List;

public interface EmployeeService {

    public Employee addNew(Employee employee);

    public List<Employee> listAll();

    public void delete(Long id);

    public void deleteEmployee(Employee employee);
}
