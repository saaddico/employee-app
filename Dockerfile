FROM openjdk:8-jdk-alpine
VOLUME /tmp
MAINTAINER Samuel Addico <saaddico@itconsortiumgh..com>
ARG JAR_FILE
COPY build/libs/*.jar /usr/local/share/ga-app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urando","-jar","/usr/local/share/ga-app.jar"]
EXPOSE 9000